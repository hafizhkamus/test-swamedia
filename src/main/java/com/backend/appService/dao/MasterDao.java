package com.backend.appService.dao;

import com.backend.appService.entity.MasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MasterDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;


    public void saveSiswa(MasterDto.Siswa param){
        String baseQuery = "insert into siswa(nama) values(:nama)";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("nama", param.getNama());

        jdbcTemplate.update(baseQuery, map);
    }

    public void saveSiswaAll(List<MasterDto.Siswa> param){
        String baseQuery = "insert into siswa(nama) values(:nama)";

        MapSqlParameterSource map = new MapSqlParameterSource();
        for (MasterDto.Siswa siswa : param){
            map.addValue("nama", siswa.getNama());

            jdbcTemplate.update(baseQuery, map);
        }
    }


    public void saveMatpel(MasterDto.MataPelajaran param){
        String baseQuery = "insert into mata_pelajaran(deskripsi) values(:desc)";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("desc", param.getDeskripsi());

        jdbcTemplate.update(baseQuery, map);
    }

    public void saveNilai(MasterDto.SaveNilai param){
        String baseQuery = "insert into nilai(id_siswa, id_mata_pelajaran, nilai) values(:idSiswa, :idMataPelajaran, :nilai)";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("idSiswa", param.getIdSiswa());
        map.addValue("idMataPelajaran", param.getIdMataPelajaran());
        map.addValue("nilai", param.getJumlahNilai());

        jdbcTemplate.update(baseQuery, map);
    }

    public List<MasterDto.Nilai> getAllNilai(){
        String baseQuery = "select n.id as id," +
                "s.nama as namaSiswa," +
                "mp.deskripsi as namaPelajaran," +
                "n.nilai as jumlahNilai" +
                "from nilai n join siswa s on(n.id_siswa = s.id) join mata_pelajaran mp on (n.id_mata_pelajaran = mp.id)";

        MapSqlParameterSource map = new MapSqlParameterSource();

        return jdbcTemplate.query(baseQuery, map, new BeanPropertyRowMapper<>(MasterDto.Nilai.class));
    }

    public List<MasterDto.Nilai> getSiswaAndAverage(){
        String baseQuery = "select s.nama as namaSiswa, avg(n.nilai) as rataan\n" +
                "from nilai n join siswa s on n.id_siswa = s.id group by nama";

        MapSqlParameterSource map = new MapSqlParameterSource();

        return jdbcTemplate.query(baseQuery, map, new BeanPropertyRowMapper<>(MasterDto.Nilai.class));
    }

}
