package com.backend.appService.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

public class MasterDto {


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Siswa{
        private Integer id;
        private String nama;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MataPelajaran{
        private Integer id;
        private String deskripsi;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Nilai{
        private Integer id;
        private Integer idSiswa;
        private String namaSiswa;
        private Integer idMataPelajaran;
        private String namaPelajaran;
        private Integer jumlahNilai;
        private BigDecimal rataan;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SaveNilai{
        private Integer id;
        private Integer idSiswa;
        private Integer idMataPelajaran;
        private Integer jumlahNilai;
    }
}
