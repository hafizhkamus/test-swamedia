package com.backend.appService;

import com.backend.appService.dao.MasterDao;
import com.backend.appService.entity.MasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Component
public class DBOperationRunner implements CommandLineRunner{

    @Autowired
    MasterDao dao;

    @Override
    public void run(String... args) throws Exception {

        //ID is default
        // JAWABAN 1 a
        dao.saveSiswa(new MasterDto.Siswa(1, "Hafizh"));

        // save all
        /*
        dao.saveSiswaAll(Arrays.asList(
                    new MasterDto.Siswa(1, "Hafizh")),
                    new MasterDto.Siswa(1, "Hafizh")),
                    new MasterDto.Siswa(1, "Hafizh")),
                    new MasterDto.Siswa(1, "Hafizh")),
                    new MasterDto.Siswa(1, "Hafizh")),
                    new MasterDto.Siswa(1, "Hafizh"))
            );
        * */

        //ID is default
        // JAWABAN 1 b
        dao.saveMatpel(new MasterDto.MataPelajaran(1, "Ilmu Pengetahuan Alam"));

        //ID is default
        //Foreign key( id siswa, id matpel)
        //JAWABAN 1 c
        //urutan (id(default), idsiswa, idmatpel, nilai)
        dao.saveNilai(new MasterDto.SaveNilai(1, 1, 1, 100));


        //getAllNilai
        //JAWABAN 1 d
        for (MasterDto.Nilai data : dao.getAllNilai()){
            System.out.print("Siswa : " + data.getNamaSiswa() + " Matpel : " + data.getNamaPelajaran() + " Nilai : " + data.getJumlahNilai());
        }

        //JAWABAN 2 C
        List<MasterDto.Nilai> get =  dao.getSiswaAndAverage();
        MasterDto.Nilai siswaTerkecil = new MasterDto.Nilai(1, 1, "", 1, "", 0, new BigDecimal(0));

        for (MasterDto.Nilai data : get){
            if (siswaTerkecil.getRataan() == new BigDecimal(0)){
                siswaTerkecil = data;
            } else {
                if(siswaTerkecil.getRataan().intValue() > data.getRataan().intValue()){
                    siswaTerkecil = data;
                }
            }
        }
        System.out.print("Siswa dengan ratan terkecil");
        System.out.print("Siswa : " + siswaTerkecil.getNamaSiswa() + " Rataan : " + siswaTerkecil.getRataan());


        System.out.println("----------All Data saved into Database----------------------");
    }
}
